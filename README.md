# listen-history-itunes

## Installation
Clone this repository.  

## Usage
Access to [https://listenhistory.herokuapp.com/api-key](https://listenhistory.herokuapp.com/api-key) and copy your `COMBINED_KEY`.  
Paste the `COMBINED_KEY` to the first line of `app.js`.  
```bash
$ npm start
```
